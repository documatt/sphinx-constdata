# developer machine deps

# tox should not be installed in current venv, but via "pipx install tox"

# please sync version with .pre-commit-config.yaml (rev attribute)

pre-commit==2.15.0

# testing
pytest==6.2.5
pytest-cov==3.0.0
# way better diffs (only with pytest -vv)
pytest-clarity==1.0.1
# nicer progress bar
pytest-sugar==0.9.4
# Type hints checker
mypy==0.910
# indirectly depends on docutils 0.17.1 coming with Sphinx
types-docutils==0.17.1
types-PyYAML==6.0.5
# import statement sorting
isort==5.10.1
# code formatter
black==21.11b1
flake8==4.0.1
# remove unused imports
pycln==1.1.0