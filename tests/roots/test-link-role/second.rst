Links to different document
===========================

Link to table from the different document (expected_link3.html):

:constdata:link:`conf2.csv?release`

Link to table from the different document with custom title (expected_link4.html):

:constdata:link:`The full project version <conf2.csv?release>`