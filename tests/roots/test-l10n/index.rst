L10N tests
==========

Table from menu
---------------

.. constdata:table:: menu_gettext.csv

.. constdata:table:: menu_gettext.json

.. constdata:table:: menu_gettext.yaml


Table from conf
---------------

.. constdata:table:: conf_gettext.csv

.. constdata:table:: conf_gettext.json

.. constdata:table:: conf_gettext.yaml

Label from menu
---------------

Please go to :constdata:label:`menu_gettext.csv?FileNew`.

Please go to :constdata:label:`menu_gettext.json?FileNew`.

Please go to :constdata:label:`menu_gettext.yaml?FileNew`.

Label from conf
---------------

It is in :constdata:label:`conf_gettext.csv?project_copyright` category.

It is in :constdata:label:`conf_gettext.json?project_copyright` category.

It is in :constdata:label:`conf_gettext.yaml?project_copyright` category.

Link from menu
--------------

Please go to :constdata:link:`menu_gettext.csv?FileSaveAs`.

Please go to :constdata:link:`menu_gettext.json?FileSaveAs`.

Please go to :constdata:link:`menu_gettext.yaml?FileSaveAs`.

Link from conf
--------------

Please go to :constdata:link:`conf_gettext.csv?project_copyright`.

Please go to :constdata:link:`conf_gettext.json?project_copyright`.

Please go to :constdata:link:`conf_gettext.yaml?project_copyright`.