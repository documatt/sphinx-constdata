msgid "Variable"
msgstr ""

msgid "Category"
msgstr ""

msgid "Description"
msgstr ""

msgid "Project information"
msgstr ""

msgid "The author name(s) of the document.  The default value is ``'unknown'``."
msgstr ""

msgid ""
"An alias of ``copyright``.\n"
"\n"
".. versionadded:: 3.5"
msgstr ""

msgid "Path"
msgstr ""

msgid "File --> Create and &open new file"
msgstr ""

msgid "File --> Save As..."
msgstr ""