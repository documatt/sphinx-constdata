L10N with msgctxt tests
=======================

Table from menu
---------------

.. constdata:table:: menu_gettext.csv

Table from conf
---------------

.. constdata:table:: conf_gettext.csv

Label from menu
---------------

Please go to :constdata:label:`menu_gettext.csv?FileNew`.

Label from conf
---------------

It is in :constdata:label:`conf_gettext.csv?project_copyright` category.

Link from menu
--------------

Please go to :constdata:link:`menu_gettext.csv?FileSaveAs`.

Link from conf
--------------

Please go to :constdata:link:`conf_gettext.csv?project_copyright`.