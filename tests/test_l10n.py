import re
from io import StringIO
from pathlib import Path

import pytest
from sphinx.application import Sphinx
from sphinx.testing.path import path

from sphinxcontrib.constdata.l10n import (
    FlatfileMessageCatalogBuilder,
    gettext,
    resolve_msgctxt_template,
)
from sphinxcontrib.constdata.settings import (
    CONFIG_POT_COMMENTS,
    CONFIG_POT_LOCATION,
    CONFIG_POT_LOCATION_FILE,
    CONFIG_POT_LOCATION_FILE_RECORD,
    CONFIG_POT_LOCATION_NO,
    CONFIG_POT_MSGCTXT,
    Settings,
)
from sphinxcontrib.constdata.utils import ConstdataError
from tests.conftest import assert_file_contains_fragment


class TestSettingPotLocation:
    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_LOCATION: CONFIG_POT_LOCATION_FILE},
    )
    def test_pot_location_file(self, app: Sphinx):
        location = "foo.csv"
        recno = 5
        builder = FlatfileMessageCatalogBuilder(app)
        actual = builder._render_location(location, recno)

        assert [(location, None)] == actual

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_LOCATION: CONFIG_POT_LOCATION_FILE_RECORD},
    )
    def test_pot_location_file_recno(self, app: Sphinx):
        location = "foo.csv"
        recno = 5
        builder = FlatfileMessageCatalogBuilder(app)
        actual = builder._render_location(location, recno)

        assert [(location, recno)] == actual

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_LOCATION: CONFIG_POT_LOCATION_NO},
    )
    def test_pot_location_nothing(self, app: Sphinx):
        location = "foo.csv"
        recno = 5
        builder = FlatfileMessageCatalogBuilder(app)
        actual = builder._render_location(location, recno)

        assert [] == actual

    @pytest.mark.sphinx(
        "html", testroot="l10n", confoverrides={CONFIG_POT_LOCATION: "invalid_value"}
    )
    @pytest.mark.xfail(raises=ConstdataError)
    def test_pot_location_invalid_value(self, app: Sphinx):
        location = "foo.csv"
        recno = 5
        builder = FlatfileMessageCatalogBuilder(app)
        builder._render_location(location, recno)


class TestSettingPotComments:
    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_COMMENTS: ("foo", "bar")},
    )
    def test_no_keys(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)
        assert "foo" == builder._render_header_comment(file=10)
        assert "bar" == builder._render_nonheader_comment(
            location=10, id_col_name="foo", id_col_value="bar"
        )

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={
            CONFIG_POT_COMMENTS: (
                "{file}",
                "{file}:{id_col_name}:{id_col_value}",
            )
        },
    )
    def test_keys(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)
        assert "10" == builder._render_header_comment(file=10)
        assert "10:foo:bar" == builder._render_nonheader_comment(
            location=10, id_col_name="foo", id_col_value="bar"
        )

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={
            CONFIG_POT_COMMENTS: ("{invalid} placeholder", "{also-invalid}")
        },
    )
    def test_invalid_value(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)

        with pytest.raises(ConstdataError):
            builder._render_header_comment(file=10)

        with pytest.raises(ConstdataError):
            builder._render_nonheader_comment(
                location=10, id_col_name="foo", id_col_value="bar"
            )


class TestSettingPotMsgctxt:
    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_MSGCTXT: None},
    )
    def test_no_msgctxt(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)
        assert None == builder._render_context(file=10, id_col_value="foo")

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_MSGCTXT: "{file}:{id_col_value}"},
    )
    def test_keys(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)
        assert "10:foo" == builder._render_context(file=10, id_col_value="foo")

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={CONFIG_POT_MSGCTXT: "{invalid} placeholder"},
    )
    def test_invalid_value(self, app: Sphinx):
        builder = FlatfileMessageCatalogBuilder(app)

        with pytest.raises(ConstdataError):
            builder._render_context(file=10, id_col_value="bar")


# @pytest.mark.xfail(reason="Locally passes, on GitLab CI failed with 'assert 0 == 1'")
@pytest.mark.sphinx("gettext", testroot="l10n")
def test_default_pot_generation(app: Sphinx, status, warning):
    """Properly extract translatable from all flatfiles messages to .pot, not only those used in documents"""
    app.build()

    assert_file_contains_fragment(
        Path(app.outdir, "constdata.pot"),
        Path(app.srcdir, "expected_pot_fragment.pot"),
    )


# @pytest.mark.xfail(
#     reason="""Sometimes causes
#             sphinx.errors.SphinxError: This environment is incompatible with the selected builder, please choose another doctree directory.
#         Sometimes not. Asked in sphinx-dev
#         https://groups.google.com/u/1/g/sphinx-dev/c/ybAgTEl4GyU, but still not sure how to fix it.
#     """
# )
@pytest.mark.sphinx("html", testroot="l10n", confoverrides={"language": "cs"})
def test_translation(app: Sphinx, status, warning):
    """Translation works. Test that translatable CSV/JSON/YAML table/link/label produce the same output."""
    _test_translation(app)


@pytest.mark.sphinx(
    "html",
    testroot="l10n-msgctxt",
    confoverrides={"language": "cs", CONFIG_POT_MSGCTXT: "{file}:{id_col_value}"},
)
def test_translation_with_msgctxt(app: Sphinx, status, warning):
    """Translation works. Test that translatable CSV/JSON/YAML table/link/label produce the same output."""
    _test_translation(app)


def _test_translation(app):
    app.build()

    assert_file_contains_fragment(
        Path(app.outdir, "index.html"), Path(app.srcdir, "expected_menu_tables.html")
    )
    assert_file_contains_fragment(
        Path(app.outdir, "index.html"), Path(app.srcdir, "expected_conf_tables.html")
    )

    assert_file_contains_fragment(
        Path(app.outdir, "index.html"),
        Path(app.srcdir, "expected_menu_links.html"),
    )
    assert_file_contains_fragment(
        Path(app.outdir, "index.html"),
        Path(app.srcdir, "expected_conf_links.html"),
    )

    assert_file_contains_fragment(
        Path(app.outdir, "index.html"),
        Path(app.srcdir, "expected_menu_labels.html"),
    )
    assert_file_contains_fragment(
        Path(app.outdir, "index.html"),
        Path(app.srcdir, "expected_conf_labels.html"),
    )


@pytest.mark.sphinx(
    "gettext",
    testroot="l10n-separate-layout/source",
    builddir=path("tests/roots/l10n-separate-layout/build/"),
)
def test_separate_layout(app: Sphinx, warning: StringIO):
    """Tests that bug causing 'does not start with' error has been fixed' exception"""
    app.build()

    warning_str = warning.getvalue()

    # e.g.
    # ValueError: '/Users/libor/git/documatt/sphinxcontrib-constdata/tests/roots/test-l10n-separate-layout/build/gettext/constdata.pot' does not start with '/Users/libor/git/documatt/sphinxcontrib-constdata/tests/roots/test-l10n-separate-layout/source'
    r = r"^ValueError: '.+constdata\.pot' does not start with '.+'$"
    assert not re.match(r, warning_str)


def test_resolve_msgctxt_template_ok():
    expected = "foo:bar"
    actual = resolve_msgctxt_template("{file}:{id_col_value}", "foo", "bar")
    assert expected == actual


def test_resolve_msgctxt_template_invalid_placeholder():
    with pytest.raises(ConstdataError):
        resolve_msgctxt_template("{invalid}:{also-invalid}", "foo", "bar")


class TestGettext:
    @pytest.mark.sphinx(
        "html",
        testroot="l10n-msgctxt",
        confoverrides={CONFIG_POT_MSGCTXT: "{file}:{id_col_value}", "language": "cs"},
    )
    def test_with_msgctxt(self, app: Sphinx):
        app.build()

        assert app.env
        settings = Settings(app.env)
        rel_path = "menu_gettext.csv"

        _ = gettext(settings, rel_path)

        # With "" (translatable string found in header row)
        assert _("_('Path')", "") == "Cesta"

        # With ID column value
        assert (
            _("_('File --> Save As...')", "FileSaveAs") == "Soubor --> Uložit jako..."
        )

    @pytest.mark.sphinx(
        "html",
        testroot="l10n",
        confoverrides={"language": "cs"},
    )
    def test_no_msgctxt_configured(self, app: Sphinx):
        app.build()

        assert app.env
        settings = Settings(app.env)
        rel_path = "menu_gettext.csv"

        _ = gettext(settings, rel_path)

        # With "" (translatable string found in header row)
        assert _("_('Path')", "") == "Cesta"

        # With ID column value
        assert (
            _("_('File --> Save As...')", "FileSaveAs") == "Soubor --> Uložit jako..."
        )
