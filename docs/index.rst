Welcome to |project|'s documentation!
=====================================

.. toctree::
   :maxdepth: 2
   :hidden:

   quickstart
   install
   conf
   flatfiles
   label
   table
   link
   l10n
   contributing

.. include:: ../README.rst
   :start-after: the rest of file included in docs/index.rst

..
   Indices and tables
   ==================

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`