.. |project| replace:: sphinxcontrib-constdata

.. image:: logo.svg
   :align: right

#########
|project|
#########

.. image:: https://gitlab.com/documatt/sphinxcontrib-constdata/badges/master/pipeline.svg
    :target: https://gitlab.com/documatt/sphinxcontrib-constdata/

.. the rest of file included in docs/index.rst

|project| is the extension for `Sphinx documentation <https://www.sphinx-doc.org/>`_ projects that allows showing values, listing tables, and generating links from CSV, JSON and YAML files. Instead of hard-coding values in the text, you write constants or queries replaced with value(s) from the external file(s). If the file contains language-sensitive strings, they can be translated with the rest of the Sphinx docs.

* docs: https://documatt.gitlab.io/sphinxcontrib-constdata
* code: https://gitlab.com/documatt/sphinxcontrib-constdata where issues and contributions are welcome

*****
About
*****

|project| started from the urge to manage thousands of external strings like UI labels, keyboard shortcuts, etc., used in a large enterprise application's docs. Devs and translators often change these strings. Idea of hard-coding these strings in the docs, and manually watching changes and updating them across the docs, scared me so much that I developed this Sphinx extension.

.. tip:: You can read about my other Sphinx documentation tools and tech writing tips, tricks, and insights at `Tech writer at work <https://techwriter.documatt.com>`_ blog.

*********
Impressum
*********

|project| is open source licensed under BSD3.

Project logo and icon "table-multiple" provided by `materialdesignicons.com <https://materialdesignicons.com/icon/table-multiple>`_.


